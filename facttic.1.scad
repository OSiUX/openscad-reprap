l=17;
a=6;
s=5;
l4=l*2+s*2;
l2=l*2+s*2;
b=l*7+s*9;

module c1 ()
{
	cube ([l,l,a]);
}

module c4 ()
{

c1();
translate ([l+s,0,0]) c1();
translate ([0,l+s,0]) c1();
translate ([l+s,l+s,0]) c1();

}

module c3 ()
{

c1();
translate ([l+s,0,0]) c1();
translate ([0,l+s,0]) c1();
$fn = 64;
translate ([l+s+l/2,l+s+l/2,0]) cylinder(a, d=l);

}    

module c6 () {
	for ( i = [0 : 5] ) {
	translate ([(l+s)*i,0,0]) c1();
	}
}

module c7() {
	for ( i = [0 : 6] ) {
	translate ([0,(l+s)*i,0]) c1();
	}
}

module glider () {
c3();   
translate ([l4,0,0]) c3();
translate ([l4*2,0,0]) c3();
translate ([0,l4,0]) c4();
translate ([l4,l4,0]) c4();
translate ([l4*2,l4,0]) c3();
translate ([l4,l4*2,0]) c3();
translate ([l4*2,l4*2,0]) c4();
translate ([0,l4*2,0]) c4();
translate ([0,l4*3,0]) c6();
translate ([l4*3,0,0]) c7();
}

module base () {
translate ([-l/2, -l/2,0]) cube([b,b,a-2]);
}

difference() {
base();
translate ([0,0,-1]) glider();
}
