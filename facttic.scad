// This script comes with ABSOLUTELY NO WARRANTY, use at own risk
// Copyright (C) 2015 Osiris Alejandro Gomez <osiris@gcoop.coop>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

l=12;
a=6;
s=4;
l4=l*2+s*2;
l2=l*2+s*2;
b=l*7+s*9;
x2=l+s;
x4=l*3+s*3;
x6=l*5+s*5;
y2=l+s;
y4=x4;
y6=x6;

module c1 () {
    cube ([l,l,a]);
}

module cubos() {
    for ( i = [0 : 6] ) {
        for ( j = [0 : 6] ) {
            translate ([(l+s)*i,(l+s)*j],0) c1();
        }
    }
}

module huecos() {
    translate ([x2-1,y2-1,-1]) cube(l+2,l+2,a+2);
    translate ([x4-1,y2-1,-1]) cube(l+2,l+2,a+2);
    translate ([x6-1,y2-1,-1]) cube(l+2,l+2,a+2);
    translate ([x6-1,y4-1,-1]) cube(l+2,l+2,a+2);
    translate ([x4-1,y6-1,-1]) cube(l+2,l+2,a+2);
}

module circulos ()
{
    $fn = 64;
    translate ([x2+l/2,y2+l/2,0]) cylinder(a, d=l);
    translate ([x4+l/2,y2+l/2,0]) cylinder(a, d=l);
    translate ([x6+l/2,y2+l/2,0]) cylinder(a, d=l);
    translate ([x6+l/2,y4+l/2,0]) cylinder(a, d=l);
    translate ([x4+l/2,y6+l/2,0]) cylinder(a, d=l);
}

module glider() {
    difference() {
        cubos();
        huecos();
    }
    circulos();
}

module base () {
    translate ([-l/2, -l/2,0]) cube([b,b,a-2]);
}

module facttic () {
    difference() {
        translate ([l/2,l/2, 0]) base();
        translate ([l/2,l/2,-1]) glider();
    }
}

facttic();

