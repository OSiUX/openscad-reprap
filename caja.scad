// This script comes with ABSOLUTELY NO WARRANTY, use at own risk
// Copyright (C) 2015 Osiris Alejandro Gomez <osiris@gcoop.coop>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use <gcoop.scad>
use <facttic.scad>

module caja () {
    union() {
        cube([120,120,5]);

        translate ([4,0,0])
            rotate ([0,-90,0]) gcoop_cuadrado();

        translate ([120,0,0])
            rotate ([0,-90,0]) gcoop_cuadrado();

        rotate ([90,0,0]) facttic();

        translate ([120,120,0])
            mirror ([1,0,0])
            rotate ([90,0,0]) facttic();
    }
}

rotate ([0,-90,0]) caja();

