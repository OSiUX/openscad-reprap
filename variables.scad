// This script comes with ABSOLUTELY NO WARRANTY, use at own risk
// Copyright (C) 2015 Osiris Alejandro Gomez <osiris@gcoop.coop>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

h=6;
r1=1.4;
r2=1.85;
r3=2.20;
r4=2.84;
r5=3.55;
d1=20.45;
d2=25.55;
d3=31.95;
d4=39.95;
d5=50;
a1=15;
a2=30;
