Generar un archivo para imprimir:

    0. Escribir un *.scad para OpenSCAD
    1. Convertir un *.scad a *.stl usando OpenSCAD
    2. Convertir un *.stl a *.gcode usando Slic3r
    3. La impresora lee únicamente *.gcode

Agregar al ~/.vimrc:

    augroup filetypedetect
        " scad -> png
        au BufNewFile,BufRead *.scad map <F3> :w<CR>:!clear;openscad -o scad.png --imgsize=800,600 --projection=p % && pqiv scad.png<CR>
        " scad -> stl
        au BufNewFile,BufRead *.scad map <F4> :w<CR>:!clear;openscad -o scad.stl % && head scad.stl<CR>
        " scad -> stl -> gcode && yagv
        au BufNewFile,BufRead *.scad map <F5> :w<CR>:!clear;openscad -o scad.stl % && slic3r -o scad.gcode scad.stl && yagv scad.gcode<CR>
        " view gcode
        au BufNewFile,BufRead *.gcode map <F4> :w<CR>:!yagv %<CR>
    augroup END

